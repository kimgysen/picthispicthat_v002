
//Manage upload in the home page 
$(document).ready(function(){ 
	
	$("#upload-img").on("submit", function(e){
		e.preventDefault(); 
		
		var fd = new FormData($(this)[0]); 
		
		$.ajax({
		  url: 'images',
		  data: fd,
		  processData: false,
		  contentType: false,
		  type: 'POST',
		  success: function(data){
		    //Display message 
			  if(Object.keys(data)[0] == "success"){
				  $("#msg-upload").html("Upload succeeded!"); 
			  } else if(Object.keys(data[0] == "error")) {
				  $("#msg-upload").html(data.error); 
			  }
		  }
		});
	})
	
}); 