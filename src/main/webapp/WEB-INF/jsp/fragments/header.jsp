<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="header-wrapper">
	<header id="page-title">
		<a href="${ pageContext.request.contextPath }/home">
			<img width="50" style="vertical-align: middle;" src="${ pageContext.request.contextPath }/assets/images/logo.png" />
			<h1 style="display:inline;padding-left:10px;">My perfect pictures</h1>
		</a>
	</header>
	<div id="header-auth">
		<c:choose>
			<c:when test="${ sessionScope.loggedIn }">
				Logged in as <a class="btn" href="${ pageContext.request.contextPath }/user?from=${ requestScope.reqPath }"><c:out value="${ sessionScope.user.userName }" /></a>&nbsp;<a class="btn" href="${ pageContext.request.contextPath }/logout">Log out</a>
		 	</c:when>
		 	<c:otherwise>
		   		<a class="btn" href="${ pageContext.request.contextPath }/login?from=${ requestScope.reqPath }">Login</a>&nbsp;<a class="btn" href="${ pageContext.request.contextPath }/register?from=${ requestScope.reqPath }">Register</a>
		 	</c:otherwise>
		</c:choose>
	</div>
</div>