<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<nav id="main-nav">
  <ul class="horizontal-menu">
    <li><a href="${ pageContext.request.contextPath }/home" class="<c:out default="None" value="${ requestScope.reqPath == '/home' ? 'active' :''}"/>">Home</a></li>
    <li><a href="${ pageContext.request.contextPath }/kittens" class="<c:out default="None" value="${ requestScope.reqPath == '/kittens' ? 'active' :''}"/>">Secret kittens</a></li>
    <li><a href="${ pageContext.request.contextPath }/computers" class="<c:out default="None" value="${ requestScope.reqPath == '/computers' ? 'active' :''}"/>">Secret computers</a></li>
    <li><a href="${ pageContext.request.contextPath }/about-us" class="<c:out default="None" value="${ requestScope.reqPath == '/about-us' ? 'active' :''}"/>">About us</a>
  </ul>
</nav>
