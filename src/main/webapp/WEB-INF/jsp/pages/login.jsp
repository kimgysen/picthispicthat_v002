<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <jsp:body>
    	<div class="box">
			<h1>Log in</h1>
			<form method="POST">
				<table>
					<tr>
						<td>Username:</td>
						<td><input type="text" name="user_name" value="${ form.userName }" />
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type="password" name="password" />
					</tr>
					<tr>
						<td></td>
						<td>
							<input type="submit" class="btn" value="Login" />
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<input type="checkbox" name="remember_me" id="remember_me" />
							<label for="remember_me">Remember me</label>
						</td>
					</tr>
				</table>
			    <c:if test="${ (not empty form) && (not form.authenticated) }">
					<span class="error ">Login failed</span>
			    </c:if>
			</form>
		    <p>
			    <a class="btn" href="<c:out value='${ pageContext.request.contextPath }/register?from=${ requestScope.reqPath }' />">Register</a>
			</p>
			<p>
		    	<a class="btn" href="<c:out value='${ pageContext.request.contextPath }${ requestScope.redirPath }' />">Get me out of here</a>
		    </p>
    	</div>
    </jsp:body>
</t:master>