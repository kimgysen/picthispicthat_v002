<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <jsp:body>
		<jsp:include page="/WEB-INF/jsp/fragments/navigation.jsp" />
    	<div class="box">
			<h1>About us</h1>
			<p>
				I'm a Java student at Intec Brussels. <br />
			</p>
			<p>
				Contact: kim.gysen@gmail.com
			</p>
    	</div>
    </jsp:body>
</t:master>