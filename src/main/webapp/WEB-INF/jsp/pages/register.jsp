<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <jsp:body>
    	<div class="box">
			<h1>Register</h1>
			<form method="POST">
				<table>
					<tr>
						<td>First name:</td>
						<td><input type="text" name="given_name" value="${ form.givenName }" />&nbsp;<span class="validation-info"><i>max 100</i></span></td>
					</tr>
					<tr>
						<td>Last name:</td>
						<td><input type="text" name="last_name" value="${ form.lastName }" />&nbsp;<span class="validation-info"><i>max 100</i></span></td>
					</tr>
					<tr>
						<td>Nickname:</td>
						<td><input type="text" name="user_name" value="${ form.userName }" />&nbsp;<span class="validation-info"><i>max 100</i></span></td>
					</tr>
					<tr>
						<td>Address:</td>
						<td><input type="text" name="address" value="${ form.address }" />&nbsp;<span class="validation-info"><i>max 100</i></span></td>
					</tr>
					<tr>
						<td>Zip:</td>
						<td><input type="text" name="zip" value="${ form.zip }" />&nbsp;<span class="validation-info"><i>max 10</i></span></td>
					</tr>
					<tr>
						<td>City:</td>
						<td><input type="text" name="city" value="${ form.city }" />&nbsp;<span class="validation-info"><i>max 100</i></span></td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type="password" name="password" />&nbsp;<span class="validation-info"><i>max 50</i></span></td>
					</tr>
					<tr>
						<td>Password repeat:</td>
						<td><input type="password" name="password_repeat" />&nbsp;<span class="validation-info"><i>max 50</i></span></td>
					</tr>
					<tr>
						<td></td>
						<td><input class="btn" type="submit" value="Register" />
					</tr>
				</table>
			    <c:if test="${ (not empty form) && (not form.valid) }">
					<div class="error">${ error }</div>
			    </c:if>
			</form>
			<i>Note: all fields are required</i>
		    <p>
		    	<a class="btn" href="<c:out value='${ pageContext.request.contextPath }${ requestScope.redirPath }' />">Get me out of here</a>
			</p>
	    </div>
    </jsp:body>
</t:master>