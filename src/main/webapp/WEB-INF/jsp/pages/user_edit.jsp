<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <jsp:body>
	    <div class="box">
			<h1>User profile</h1>
			<form method="POST">
				<table>
					<tr>
						<td>User name</td>
						<td><c:out value='${ sessionScope.user.userName }' /></td>
					</tr>
					<tr>
						<td>First name</td>
						<td><input type="text" name="given_name" value="<c:out value='${ sessionScope.user.givenName }' />" />&nbsp;<span class="validation-info"><i>max 100</i></span></td>
					</tr>
					<tr>
						<td>Last name</td>
						<td><input type="text" name="last_name" value="<c:out value='${ sessionScope.user.lastName }' />" />&nbsp;<span class="validation-info"><i>max 100</i></span></td>
					</tr>
					<tr>
						<td>Address</td>
						<td><input type="text" name="address" value="<c:out value='${ sessionScope.user.address }' />" />&nbsp;<span class="validation-info"><i>max 100</i></span></td>
					</tr>
					<tr>
						<td>Zip</td>
						<td><input type="text" name="zip" value="<c:out value='${ sessionScope.user.zip }' />" />&nbsp;<span class="validation-info"><i>max 10</i></span></td>
					</tr>
					<tr>
						<td>City</td>
						<td><input type="text" name="city" value="<c:out value='${ sessionScope.user.city }' />" />&nbsp;<span class="validation-info"><i>max 100</i></span></td>
					</tr>
					<tr>
						<td></td>
						<td><a class="btn" href="${ pageContext.request.contextPath }/user?from=${ param.from }">Cancel edit</a>&nbsp;<input class="btn" type="submit" /></td>
					</tr>
				</table>
			    <c:if test="${ (not empty form) && (not form.valid) }">
					<div class="error">${ error }</div>
			    </c:if>
			</form>
			<p>
				<i>Note: all fields are required</i>
			</p>
			<br />
		    <p>
			    <a class="btn" href="<c:out value='${ pageContext.request.contextPath }${ requestScope.redirPath }' />">Get me out of here</a>
		    </p>    
	    </div>
    </jsp:body>
</t:master>