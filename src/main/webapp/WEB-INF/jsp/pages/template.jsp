<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <jsp:body>
		<jsp:include page="/WEB-INF/jsp/fragments/navigation.jsp" />
		<div id="gallery-toolbar" class="box">
			<button id="btn-generate" name="generate" class="btn">Random images</button>
			<c:choose>
			    <c:when test="${ sessionScope.loggedIn && requestScope.visibility=='public' }">
					<form id="upload-img" method="post" action="${ pageContext.request.contextPath }/images" enctype="multipart/form-data">
						<p>
							Select file to upload:
							<input type="file" name="uploadedImage" accept="image/*" /><br/><br/>
							<input id="btn-upload" class="btn" value="Upload" type="submit" class="btn" />
						</p>
						<span id="msg-upload"></span>
					</form>
			    </c:when>
			    <c:when test="${ requestScope.visibility=='public' }">
			    	<a id="btn-upload" class="btn" href="login">Upload new image</a>
			    </c:when>
			</c:choose>
		</div>
		<ul id="grid" class="grid effect-2">
			<c:forEach items="${ requestScope.pictures }" var="pic">
				<li class="item">
				  <a target="_blank" href="<c:url value='${ requestScope.filePath }${ pic.fileName }' />">
				    <img src="<c:url value='${ requestScope.filePath }${ pic.fileName }' />" alt="${ pic.fileName }" />
				  </a>
				</li>
			</c:forEach>
		</ul>
		<script src="<c:url value="assets/js/masonry/masonry-min.js" />"></script>
		<script src="<c:url value="assets/js/masonry/imagesloaded.js" />"></script>
		<script src="<c:url value="assets/js/masonry/classie.js" />"></script>
		<script src="<c:url value="assets/js/masonry/AnimOnScroll.js" />"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
		<script>
			//Reload the current page 
			$("#btn-generate").on("click", function(){
				//Simple page regeneration 
				window.location.reload(); 
			}); 
						
			//Fetch more images using ajax --> to be done
			/*$("#load-more-images").on("click", function(){ 
				jQuery.ajax({
				    type: "GET", 
				    url: "/images", 
				    dataType: "json",
				    success: function(results){
				    	console.log(results); 
				    }, 
				    error: function(XMLHttpRequest, textStatus, errorThrown){
				        alert("Error");
				    }
				});				
				
			}); */
			
			//On scroll, load new images 
	        new AnimOnScroll( document.getElementById( 'grid' ), {
	            minDuration : 0.4,
	            maxDuration : 0.7,
	            viewportFactor : 0.2
	        } );
		</script>
		<script src="assets/js/upload-file.js"></script>
    </jsp:body>
</t:master>