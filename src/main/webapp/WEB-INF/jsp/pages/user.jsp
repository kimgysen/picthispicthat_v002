<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <jsp:body>
    	<div class="box">
		<h1>User profile</h1>
			<table>
				<tr>
					<td>User name</td>
					<td><c:out value="${ user.userName }" /> </td>
				</tr>
				<tr>
					<td>First name</td>
					<td>${ user.givenName } </td>
				</tr>
				<tr>
					<td>Last name</td>
					<td>${ user.lastName } </td>
				</tr>
				<tr>
					<td>Address</td>
					<td>${ user.address } </td>
				</tr>
				<tr>
					<td>Zip</td>
					<td>${ user.zip } </td>
				</tr>
				<tr>
					<td>City</td>
					<td>${ user.city } </td>
				</tr>
			</table>
			<p>
				<a class="btn" href="${ pageContext.request.contextPath }/user/edit?from=${ requestScope.redirPath }">Edit profile</a>
			</p>
			<br />
			<p>
				<a class="btn" href="${ pageContext.request.contextPath }${ requestScope.redirPath }">Get me out of here!</a>
			</p>
    	</div>
    </jsp:body>
</t:master>