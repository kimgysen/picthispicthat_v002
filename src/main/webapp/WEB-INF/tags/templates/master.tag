<%@ tag description="Master template" pageEncoding="UTF-8" %>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Insert title here</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/reset.css"></link>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css"></link>
		<script src="${pageContext.request.contextPath}/assets/js/modernizr/modernizr.custom.js"></script>
	</head>
	<body>
    	<div id="wrapper">
    		<div id="page-header">
      			<jsp:include page="/WEB-INF/jsp/fragments/header.jsp" />
    		</div>
			<div id="page-body">
	      		<jsp:doBody/>
			</div>
	    	<div id="page-footer" class="box">
	     		Created by Kim Gysen 
	    	</div>
    	</div>
  	</body>
</html>