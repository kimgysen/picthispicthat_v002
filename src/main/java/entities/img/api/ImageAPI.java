package entities.img.api;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URLDecoder;
import java.nio.file.Files;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.lang.SerializationUtils;

import entities.img.beans.Image;
import entities.img.repo.ImageRepository;
import entities.user.beans.User;

/**
 * Servlet that acts as a resource API 
 * @author kimgysen
 *
 */
@WebServlet("/images/*")
@MultipartConfig(
		location="/", 
		fileSizeThreshold=1024*1024, 
		maxFileSize=1024*1024*1+(1024*1024)/5, 
		maxRequestSize=1024*1024*5*5)
public class ImageAPI extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Gets and returns an image 
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
        // Get requested image by path info.
        String requestedImage = req.getPathInfo();
        
        // Check if file name is actually supplied to the request URI.
        if (requestedImage == null) {
            // Do your thing if the image is not supplied to the request URI.
            // Throw an exception, or send 404, or show default/warning image, or just ignore it.
            resp.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
            return;
        }
        
        // Decode the file name (might contain spaces and on) and prepare file object.
        File image = new File(getServletContext().getInitParameter("upload.location"), URLDecoder.decode(requestedImage, "UTF-8"));
        
        // Check if file actually exists in filesystem.
        if (!image.exists()) {
            // Do your thing if the file appears to be non-existing.
            // Throw an exception, or send 404, or show default/warning image, or just ignore it.
            resp.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
            return;
        }
        
        // Get content type by filename.
        String contentType = getServletContext().getMimeType(image.getName());

        // Check if file is actually an image (avoid download of other files by hackers!).
        // For all content types, see: http://www.w3schools.com/media/media_mimeref.asp
        if (contentType == null || !contentType.startsWith("image")) {
            // Do your thing if the file appears not being a real image.
            // Throw an exception, or send 404, or show default/warning image, or just ignore it.
            resp.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
            return;
        }
        
        // Init servlet response.
        resp.reset();
        resp.setContentType(contentType);
        
        // Write image content to response.
        Files.copy(image.toPath(), resp.getOutputStream());        
	}
	
	/**
	 * Receives file upload, sends back json reply 
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException { 
        
        // constructs path of the directory to save uploaded file
        String savePath = getServletContext().getInitParameter("upload.location"); 
         
        // creates the save directory if it does not exists
        File fileSaveDir = new File(savePath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdir();
        }
        
        String jsonObj = null; 
		//Get file 
		for (Part part : req.getParts()) {
		    String fileName = extractFileName(part);
			//Limit file type to images 
			String mimeType = getServletContext().getMimeType(fileName);
			//Note: this isn't an actual check on file content, just a basic check for sake of the exercise 		
			if (mimeType.startsWith("image/")) { 
			    // It's an image: 
				Image image = new Image(); 
				image.setFileName(fileName); 
		    	//Deep copy is required to avoid modifying the original User object 
		    	User user = (User) SerializationUtils.clone((Serializable) req.getSession().getAttribute("user")); 
			    image.setUser( user ); 
			    image.setFsPath(fileSaveDir.getPath() + "/" + fileName); 
			    image.setImage(part); 
			    image.setPrivate(false); //The api allows setting privacy, but the app doesn't use it: uploads = public 
			    
			    ImageRepository imgRepo = new ImageRepository(); 
			    imgRepo.save(image); 
			    jsonObj = "{\"success\": \"Image successfully saved\"}"; 
			} else {
				jsonObj = "{\"error\": \"File is not an image\"}"; 
			}
		} 
		
        //Return json 
		resp.setContentType("application/json");
		// Get the printwriter object from response to write the required json object to the output stream      
		PrintWriter out = resp.getWriter();
		// Assuming your json object is **jsonObject**, perform the following, it will return your json object  
		out.print(jsonObj);
		out.flush();
	}
	
    /**
     * Extracts file name from HTTP header content-disposition
     */
	private String extractFileName(Part part) {
	    String contentDisp = part.getHeader("content-disposition");
	    String[] items = contentDisp.split(";");
	    for (String s : items) {
	        if (s.trim().startsWith("filename")) {
	            return s.substring(s.indexOf("=") + 2, s.length()-1);
	        }
	    }
	    return "";
	}	
}
