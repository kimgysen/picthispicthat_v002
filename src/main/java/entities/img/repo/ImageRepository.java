package entities.img.repo;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import config.Database;
import entities.img.beans.Image;

/**
 * Image repository implementation 
 * @author kimg
 *
 */
public class ImageRepository implements IImageRepository{
	
	EntityManager entityMgr; 
	
	/**
	 * Allow multiple types of databases 
	 */
	public ImageRepository(){ 
		entityMgr = Database.getEntityManager(); 
	}
	
    public List<Image> getRandom(int nr_pics, int categoryId) {
    	List<Image> images = null; 
    	
		TypedQuery<Image> q = entityMgr.createQuery("select distinct i from Image as i where i.categoryId=:categoryId ORDER BY RAND()", Image.class); 
		q.setMaxResults(nr_pics); 
		q.setParameter("categoryId", (short) categoryId); 
		images = (List<Image>) q.getResultList(); 
		
        return images; 
    } 
    
    public Image find(Image image) {
        return null;
    }
    
    public long save(Image image) throws IOException { 
        image.getImage().write(image.getFsPath());
    	EntityTransaction tx = entityMgr.getTransaction(); 
    	tx.begin(); 
    	entityMgr.persist(image); 
    	tx.commit(); 
    	
		return image.getId(); 
    }
    
    public void delete(Image image) {
    	//Not required 
    }
    
}
