package entities.img.repo;

import java.io.IOException;
import java.util.List;

import entities.img.beans.Image;

/**
 * Interface for Image repository 
 * @author kimg 
 * 
 * Definition: 
 * A Repository mediates between the domain and data mapping layers, acting like an in-memory domain object collection. 
 * Client objects construct query specifications declaratively and submit them to Repository for satisfaction. 
 * Objects can be added to and removed from the Repository, as they can from a simple collection of objects, 
 * and the mapping code encapsulated by the Repository will carry out the appropriate operations behind the scenes
 * 
 */
public interface IImageRepository {
	Image find(Image image); 
    long save(Image image) throws IOException; 
    void delete(Image image); 
    
    List<Image> getRandom( int nr_pics, int categoryId ); 
}
