package entities.img.repo;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import config.Database;
import entities.img.beans.Image;

/**
 * Image repository implementation 
 * @author kimg
 *
 */
public class ImageRepository_JDBC implements IImageRepository{
	
	DataSource ds; 
	
	/**
	 * Allow multiple types of databases 
	 */
	public ImageRepository_JDBC(){ 
		ds = Database.getDataSource(); 
	}
	
    public List<Image> getRandom(int nr_pics, int categoryId) {
        String strQuery = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<Image> list = null;
        try {
            conn = ds.getConnection();
            strQuery = "SELECT * FROM ImageTbl WHERE category_id = ? ORDER BY RAND() LIMIT 0, ?;";
            pstmt = conn.prepareStatement(strQuery);
            pstmt.setInt(1, categoryId);
            pstmt.setInt(2, nr_pics);
            rs = pstmt.executeQuery();
            list = this.resultSetToList(rs);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if(conn != null) conn.close();
                if(rs != null) rs.close();
                if(pstmt != null) pstmt.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public Image find(Image image) {
        return null;
    }

    public long save(Image image) throws IOException {
        image.getImage().write(image.getFsPath());
        long id = 0;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = ds.getConnection();
            String strQuery = null;
            strQuery = "INSERT INTO ImageTbl(id, name, user_id, category_id, private) VALUES(null, ?, ?, ?, ?)";
            pstmt = conn.prepareStatement(strQuery, 1);
            pstmt.setString(1, image.getFileName());
            pstmt.setLong(2, image.getUser().getId());
            pstmt.setInt(3, 0);
            pstmt.setBoolean(4, image.isPrivate());
            pstmt.executeUpdate();
            rs = pstmt.getGeneratedKeys();
            rs.next();
            id = rs.getLong(1);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if(conn != null) conn.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return id;
    }

    public void delete(Image image) {
    }

    private List<Image> resultSetToList(ResultSet rs) throws SQLException {
        ArrayList<Image> list = new ArrayList<Image>();
        while (rs.next()) {
            Image img = new Image();
            img.setId((long)rs.getInt("id"));
            img.setFileName(rs.getString("name"));
            img.setTimeStamp(rs.getTimestamp("created_at"));
            list.add(img);
        }
        return list;
    }

}
