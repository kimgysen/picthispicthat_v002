package entities.img.beans;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.servlet.http.Part;

import entities.user.beans.User;

/**
 * Image bean 
 * @author kimg
 *
 */
@Entity
@Table(name="imagetbl")
public class Image { 
	
	private long 			id; 
	private String 			fileName; 
	private String 			fsPath; 
	private Date			timeStamp; 
	private Part 			image; 
	private short 			categoryId; 
	private boolean 		bPrivate; 
	private User			user; 
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@ManyToOne(cascade=CascadeType.MERGE) 
	@JoinColumn(name = "user_id")
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Column(name="name")
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	@Transient
	public String getFsPath(){
		return fsPath; 
	}
	public void setFsPath(String fsPath){
		this.fsPath = fsPath; 
	}
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTimeStamp() { 
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	@Transient
	public Part getImage(){
		return image; 
	}
	public void setImage(Part image){
		this.image = image; 
	}
	@Column(name="category_id",columnDefinition = "SMALLINT")
	public short getCategoryId(){
		return categoryId; 
	}
	public void setCategoryId(short categoryId){
		this.categoryId = categoryId; 
	}
	@Column(name="private")
	public void setPrivate(boolean bPrivate){
		this.bPrivate = bPrivate; 
	}
	public boolean isPrivate(){
		return bPrivate; 
	}
	@Override
	public String toString() {
		return "Image [id=" + id + ", fileName=" + fileName + ", fsPath=" + fsPath + ", timeStamp=" + timeStamp
				+ ", image=" + image + ", bPrivate=" + bPrivate + ", user=" + user + "]";
	}
	
}
