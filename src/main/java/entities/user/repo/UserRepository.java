package entities.user.repo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import config.Database;
import entities.user.beans.User;

/**
 * User implementation 
 * @author kimg
 *
 */
public class UserRepository implements IUserRepository {
	
	EntityManager entityMgr; 
	
	/**
	 * Get entity manager from factory 
	 */
	public UserRepository(){ 
		entityMgr = Database.getEntityManager(); 
	}
	
	/**
	 * Find a single user based on id, username or token 
	 * If bPassword is false, password and token are not retrieved (sensitive) 
	 * @param user
	 * @param bPassword
	 * @return
	 */
	public User find(User user, boolean bPassword){  
		
    	User dbUser = null; 
		
		if(user.getId() != 0 || user.getUserName() != null || user.getToken() != null){ 
	    	
	    	if(user.getId() != 0){
		    	EntityTransaction tx = entityMgr.getTransaction(); 
		    	tx.begin(); 
		    	dbUser = entityMgr.find(User.class, user.getId()); 
		    	tx.commit(); 
	    	} else { 
	    		List<User> users = null; 
	    		if (user.getUserName() != null){ 
		    		TypedQuery<User> q = entityMgr.createQuery("select u from User as u where u.userName=:username", User.class); 
		    		q.setParameter("username", user.getUserName()); 
		    		users = (List<User>) q.getResultList(); 
	    		} else if (user.getToken() != null){ 
		    		TypedQuery<User> q = entityMgr.createQuery("select u from User as u where u.token=:token", User.class); 
		    		q.setParameter("username", user.getToken()); 
		    		users = (List<User>) q.getResultList(); 
	    		}
	    		
	    		dbUser = users.size() > 0 ? users.get(0) : null; 
		    	if(!bPassword) dbUser.setToken(null); 
		    	if(!bPassword) dbUser.setPassword(null); 
	    	} 
	    	
		}
    	
    	return dbUser; 
    	
	}
	
	@Override
	public User find(User user) { 
		return find(user, false); //Don't set password / token by default 
	}
	
	@Override
	public long save(User user) { 
    	EntityTransaction tx = entityMgr.getTransaction(); 
    	tx.begin(); 
    	entityMgr.persist(user); 
    	tx.commit(); 
    	
		return user.getId(); 
	}
	
	@Override
	public void delete(User user) {
		//Not required 
	}
	
	@Override
	public List<User> getAll() {
		return null; 
	}
	
}
