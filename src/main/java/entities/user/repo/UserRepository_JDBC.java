package entities.user.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import config.Database;
import entities.user.beans.User;

/**
 * User implementation 
 * @author kimg
 *
 */
public class UserRepository_JDBC implements IUserRepository {
	
	DataSource ds; 
	
	/**
	 * Allow multiple types of databases 
	 */
	public UserRepository_JDBC(){ 
		ds = Database.getDataSource(); 
	}
	
	/**
	 * Find a single user based on id, username or token 
	 * If bPassword is false, password and token are not retrieved (sensitive) 
	 * @param user
	 * @param bPassword
	 * @return
	 */
	public User find(User user, boolean bPassword){ 
    	
		String strQuery = null; 
		User dbUser = null; 
		
		if(user != null){ 
			if(user.getId() != 0 || user.getUserName() != null || user.getToken() != null){ 
				Connection conn = null; 
				PreparedStatement pstmt = null; 
				ResultSet rs = null; 
				
				try { 
					conn = ds.getConnection(); 
					if(user.getId() != 0){ 
						strQuery = "SELECT * FROM UserTbl WHERE id = ?"; 
						pstmt = conn.prepareStatement(strQuery);
						pstmt.setLong(1, user.getId()); 
						
					} else if ( user.getUserName() != null ){ 
						strQuery = "SELECT * FROM UserTbl WHERE user_name = ?"; 
						pstmt = conn.prepareStatement(strQuery); 
						pstmt.setString(1, user.getUserName()); 
						
					} else if (user.getToken() != null){
						strQuery = "SELECT * FROM UserTbl WHERE token = ?"; 
						pstmt = conn.prepareStatement(strQuery); 
						pstmt.setString(1, user.getToken()); 
					}
					
					rs =  pstmt.executeQuery();
				    while (rs.next()) { 
				    	dbUser = new User(); 
				    	dbUser.setId(rs.getLong("id")); 
				    	dbUser.setUserName(rs.getString("user_name"));
				    	dbUser.setGivenName(rs.getString("given_name")); 
				    	dbUser.setLastName(rs.getString("last_name"));
				    	dbUser.setAddress(rs.getString("address"));
				    	dbUser.setZip(rs.getString("zip"));
				    	dbUser.setCity(rs.getString("city")); 
				    	
				    	if(bPassword) dbUser.setToken(rs.getString("token")); 
				    	if(bPassword) dbUser.setPassword(rs.getString("password")); 
				    } 
					
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {
						if(conn != null) conn.close(); 
						
					} catch (SQLException e) {
						e.printStackTrace();
					} 
				}				
			}
			
		} 
		return dbUser; 
	}
	
	@Override
	public User find(User user) { 
		return find(user, false); //Don't set password / token by default 
	}
	
	@Override
	public long save(User user) {
		//Check if an id is set on the user, then (s)he exists in the database 
		boolean bExists = false; 
		long id = user.getId(); 
		if(id != 0) bExists = true; 
		
		Connection conn = null; 
		PreparedStatement pstmt = null; 
		ResultSet rs = null; 
		try {
			conn = ds.getConnection(); 
			String strQuery = null; 
			if(bExists){ 
				strQuery = "UPDATE UserTbl SET user_name=?, given_name=?, last_name=?, address=?, zip=?, city=?, password=?, token=? WHERE id=?;"; 
				
			} else { 
				strQuery = "INSERT INTO UserTbl(id, user_name, given_name, last_name, address, zip, city, password, token) VALUES(null, ?, ?, ?, ?, ?, ?, ?, null)"; 
			}
			
			pstmt = conn.prepareStatement(strQuery, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, user.getUserName());
			pstmt.setString(2, user.getGivenName());
			pstmt.setString(3, user.getLastName());
			pstmt.setString(4, user.getAddress());
			pstmt.setString(5, user.getZip());
			pstmt.setString(6, user.getCity());
			pstmt.setString(7, user.getPassword());
			
			if(bExists) {
				pstmt.setString(8, user.getToken()); 
				pstmt.setLong(9, user.getId()); 
			}
			
			System.out.println(pstmt);
			pstmt.executeUpdate(); 
			
			if(!bExists){
				rs = pstmt.getGeneratedKeys(); 
				rs.next(); 
				id = rs.getInt(1); 
			} 
		} catch (SQLException e) {
			e.printStackTrace();
			return 0; 
		} finally {
			try {
				if(conn != null) conn.close(); 
				
			} catch (SQLException e) {
				e.printStackTrace();
			} 
		}
		
		return id; 
	}
	
	@Override
	public void delete(User user) {
		
	}
	
	@Override
	public List<User> getAll() {
		return null; 
	}
	
}
