package entities.user.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * User bean 
 * @author kimg
 *
 */
@Entity
@Table(name="usertbl")
public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private long id; 
	private String givenName; 
	private String lastName; 
	private String userName; 
	private String address; 
	private String zip;
	private String city; 
	private String password;
	private String token; 
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	public long getId(){
		return id; 
	}
	public void setId(long id){
		this.id = id; 
	}
	@Column(name="given_name")
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	@Column(name="last_name")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Column(name="address")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Column(name="zip")
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	@Column(name="city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	} 
	@Column(name="password", columnDefinition="binary(60)")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Column(name="token")
	public String getToken(){
		return token; 
	}
	public void setToken(String token){
		this.token = token; 
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", givenName=" + givenName + ", lastName=" + lastName + ", userName=" + userName
				+ ", address=" + address + ", zip=" + zip + ", city=" + city + ", password=" + password + ", token="
				+ token + "]";
	}
	
}
