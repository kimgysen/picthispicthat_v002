package auth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import cookie.CookieUtil;
import entities.user.beans.User;
import entities.user.repo.UserRepository;

/**
 * Class containing authentication utility methods 
 * @author kimgysen
 *
 */
public class Auth {
	
	public static final String COOKIE_NAME = "remember"; 
	public static final int COOKIE_AGE = 259200; 
	
	/**
	 * Check if the user is currently authenticated 
	 * @param req
	 * @return
	 */
	public static boolean isAuthenticated(HttpServletRequest req){ 
		HttpSession session = req.getSession(false); 
		boolean bAuthenticated = false; 
		
		if(session == null || (boolean) session.getAttribute("loggedIn") == false){ 
			 //Check remember me cookie 
			 String uuid = CookieUtil.getCookieValue(req, COOKIE_NAME); 
			 if(uuid != null ){
				 User user = new User(); 
				 user.setToken(uuid); 
				 user = new UserRepository().find(user); 
				 
				 //Check if a user with token XYZ exists. 
				 if (user != null){ 
					 setUserSession(req, user); 
					 bAuthenticated = true; 
				 }
			 }
			 
		} else if( (boolean) session.getAttribute("loggedIn") == true ){
			bAuthenticated = true; 
		}
		return bAuthenticated; 
	}
	
	public static void setUserSession(HttpServletRequest req, User user){ 
		HttpSession session = req.getSession(); //Create a new session 
		session.setAttribute("loggedIn", true);
		session.setAttribute("user", user);
	}
}
