package auth.servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import auth.Auth;
import cookie.CookieUtil;

/**
 * Manage user logout 
 * @author kimg
 *
 */
@WebServlet("/logout")
public class LogoutServlet extends HttpServlet{ 
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Log the user out 
	 */
	protected void doGet(HttpServletRequest req, javax.servlet.http.HttpServletResponse resp) throws javax.servlet.ServletException ,java.io.IOException {
		HttpSession session = ((HttpServletRequest) req).getSession(false); 
		if(session != null){ 
			CookieUtil.removeCookie(resp, Auth.COOKIE_NAME); 
			session.invalidate(); 
			
		}
		resp.sendRedirect("home"); 
	};
	
	
}
