package auth.servlets;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mindrot.jbcrypt.BCrypt;

import auth.Auth;
import auth.validator.RegisterValidator;
import entities.user.beans.User;
import entities.user.repo.UserRepository;

/**
 * Manage user registration 
 * Ideally send to RESTful endpoint, but calling the repository directly here 
 * @author kimg
 *
 */
@WebServlet(urlPatterns = "/register", initParams = @WebInitParam(name="validationerror", value="false"))
public class RegisterServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	
	private static final String VIEW_NAME  = "/WEB-INF/jsp/pages/register.jsp"; 
	private static final String MODEL_NAME = "form"; 
	private static final String ERROR_NAME = "error"; 
	
	/**
	 * Get register webpage 
	 * @param req
	 * @param res
	 * @throws IOException 
	 * @throws ServletException 
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.getRequestDispatcher(VIEW_NAME).forward(req, res); 
	}
	
	/**
	 * Register: store user data 
	 * @param req
	 * @param res
	 * @throws IOException 
	 * @throws ServletException 
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException { 
		
		boolean bValidated = false; 
		//Validate password repeat 
		String password = req.getParameter("password"); 
		String passwordRepeat = req.getParameter("password_repeat"); 
		User user = null; 
		String strError = null; 
		
		//Validate input 
		user = new User(); 
		user.setGivenName(req.getParameter("given_name"));
		user.setLastName(req.getParameter("last_name"));
		user.setUserName(req.getParameter("user_name"));
		user.setAddress(req.getParameter("address")); 
		user.setZip(req.getParameter("zip")); 
		user.setCity(req.getParameter("city"));
		user.setPassword(password); 
		
		//Validator checks the user input (min - max characters, not empty)
		RegisterValidator validator = new RegisterValidator(user); 
		bValidated = validator.validate(); 
		if(!bValidated) strError = "Invalid fields: make sure that all field are filled in, and that the max amount of characters is not exceeded!"; 
		
		//Check if the password repetition is correct 
		if(bValidated){
			if(!password.equals(passwordRepeat)){ 
				bValidated = false; 
				strError = "Password repetition doesn't match."; 
			} 
		}
		
		if(bValidated){ 
			user = register(user); 
			if(user != null){ //Crucial guard stmt to check if the db insert was successful 
				Auth.setUserSession(req, user); 
	            String strFrom = req.getParameter("from");
	            if (strFrom != null) {
	                res.sendRedirect(req.getContextPath() + strFrom);
	            } else {
	                res.sendRedirect(req.getContextPath() + "/home");
	            }
			} 
			//Else check stack trace to see what went wrong 
			
		} else { 
			//Send the user back with error message 
			final SimpleForm form = map(req); 
			form.setValid(false);
			req.setAttribute(MODEL_NAME, form);
			req.setAttribute(ERROR_NAME, strError);
			req.getRequestDispatcher(VIEW_NAME).forward(req, res); 
		}
	}
	
	/**
	 * Register the user 
	 * @param user
	 * @return
	 */
	private User register(User user){ 
		//Encrypt the password with embedded salt 
		String hashed = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(12)); 
		user.setPassword(hashed); 
		long id = new UserRepository().save(user); 
		if (id != 0) 
			return user; 
		else 
			return null; 
	}
	
	/**
	 * Map request parameters to form fields 
	 * @param request
	 * @return
	 */
	private SimpleForm map(final HttpServletRequest request) {
	    SimpleForm form = new SimpleForm();
	    form.setGivenName(request.getParameter("given_name"));
	    form.setLastName(request.getParameter("last_name"));
	    form.setUserName(request.getParameter("user_name"));
	    form.setAddress(request.getParameter("address"));
	    form.setZip(request.getParameter("zip"));
	    form.setCity(request.getParameter("city"));
	    
	    return form;
	}
	
	/**
	 * Class representing the register form 
	 * @author kimg
	 *
	 */
	public class SimpleForm implements Serializable {
	    private static final long serialVersionUID = -2756917543012439177L;
	    
	    private String givenName;
	    private String lastName;
	    private String userName; 
	    private String address; 
	    private String zip; 
	    private String city; 
	    private boolean valid; 
	    
	    public String getGivenName() {
	        return givenName;
	    }
	    public void setGivenName(String givenName) {
	        this.givenName = givenName;
	    }
	    public String getLastName() {
	        return lastName; 
	    }
	    public void setLastName(String lastName) {
	        this.lastName = lastName;
	    }
	    public String getUserName() {
	        return userName;
	    }
	    public void setUserName(String userName) {
	        this.userName = userName;
	    }
	    public String getAddress(){
	    	return address; 
	    }
	    public void setAddress(String address){
	    	this.address = address; 
	    }
	    public String getZip(){
	    	return zip; 
	    }
	    public void setZip(String zip){
	    	this.zip = zip; 
	    }
	    public String getCity(){
	    	return city; 
	    }
	    public void setCity(String city){
	    	this.city = city; 
	    }
	    public void setValid(boolean valid){
	    	this.valid = valid; 
	    }
	    public boolean isValid(){
	    	return valid; 
	    }
	}	
	
}
