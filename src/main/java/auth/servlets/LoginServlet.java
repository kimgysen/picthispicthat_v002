package auth.servlets;

import java.io.IOException;
import java.io.Serializable;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mindrot.jbcrypt.BCrypt;

import auth.Auth;
import cookie.CookieUtil;
import entities.user.beans.User;
import entities.user.repo.UserRepository;

/**
 * Manage user login 
 * @author kimg
 *
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private static final String MODEL_NAME = "form"; 	
	private static final String VIEW_NAME  = "/WEB-INF/jsp/pages/login.jsp"; 
	
	/**
	 * Get login webpage 
	 * @param req
	 * @param res
	 * @throws IOException 
	 * @throws ServletException 
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.getRequestDispatcher(VIEW_NAME).forward(req, res); 
	}
	
	/**
	 * Authenticate 
	 * @param req
	 * @param res
	 * @throws IOException 
	 * @throws ServletException 
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		String username = req.getParameter("user_name"); 
		String password = req.getParameter("password"); 
		boolean bRemember = req.getParameter("remember_me") != null && "on".equals(req.getParameter("remember_me")); 
		
		User user = null; 
		UserRepository userRepo = null; 
		boolean bAuthenticated = false; 
		
		if(username == null || password == null || username.isEmpty() || password.isEmpty()){
			bAuthenticated = false; 
			
		} else { 
			user = new User(); 
			user.setUserName(username); 
			userRepo = new UserRepository(); 
			user = userRepo.find(user, true); 
			if(validate(password, user.getPassword())) bAuthenticated = true; 
		}
		
		if(bAuthenticated){ 
			//Set session auth 
			Auth.setUserSession(req, user);
			
			//Set remember me cookie 
			if(bRemember){ 
				String uuid = UUID.randomUUID().toString(); 
				user.setToken(uuid); 
				long id = userRepo.save(user); 
				if(id != 0){
					CookieUtil.addCookie(res, Auth.COOKIE_NAME, uuid, Auth.COOKIE_AGE); 
				}
			} else {
				CookieUtil.removeCookie(res, Auth.COOKIE_NAME); 
			}
			
            String strFrom = req.getParameter("from");
            if (strFrom != null) {
                res.sendRedirect(req.getContextPath() + strFrom);
            } else {
                res.sendRedirect(req.getContextPath() + "/home");
            }
			
		}else{
			//Send the user back with error message 
			final SimpleForm form = map(req); 
			form.setAuthenticated(false);
			req.setAttribute(MODEL_NAME, form);
			req.getRequestDispatcher(VIEW_NAME).forward(req, res); 
		}
	}
	
	public boolean validate(String password, String password_hashed){ 
		if(BCrypt.checkpw(password, password_hashed)){ 
			return true; 
		}else{ 
			return false; 
		}
	}
	
	private SimpleForm map(final HttpServletRequest request) {
	    SimpleForm form = new SimpleForm();
	    form.setUserName(request.getParameter("user_name"));
	    
	    return form;
	}
	
	public class SimpleForm implements Serializable {
	    private static final long serialVersionUID = -2756917543012439177L;
	    
	    private String userName;
	    private boolean authenticated; 
	    
	    public String getUserName() {
	        return userName;
	    }
	    public void setUserName(String userName) {
	        this.userName = userName;
	    }
	    public void setAuthenticated(boolean bAuthenticated){
	    	this.authenticated = bAuthenticated; 
	    }
	    public boolean isAuthenticated(){
	    	return authenticated; 
	    }
	}
}
