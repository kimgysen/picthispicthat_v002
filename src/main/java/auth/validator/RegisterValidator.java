package auth.validator;

import java.util.Arrays;

import entities.user.beans.User;

/**
 * Perform very simple validation on the values uploaded by the registration form. 
 * (This exercise is not about security) 
 * @author kimgysen 
 *
 */
public class RegisterValidator{
	
	User user = null; 
	
	/**
	 * Validates injected user 
	 * @param user
	 */
	public RegisterValidator(User user) {
		this.user = user; 
	}
	
	/**
	 * Default validator 
	 * @return
	 */
	public boolean validate(){ 
		return validate(new String[]{"given_name", "last_name", "user_name", "address", "city", "zip", "password"}); 
	}
	
	/**
	 * Overloaded validator 
	 * @return
	 */
	public boolean validate(String... fields){ 
		
		boolean bValidate = true; 
		
		boolean bUserName = validateCharCount(user.getUserName(), 1, 100); 
		boolean bGivenName = validateCharCount(user.getGivenName(), 1, 100); 
		boolean bLastName = validateCharCount(user.getLastName(), 1, 100); 
		boolean bAddress = validateCharCount(user.getAddress(), 1, 100); 
		boolean bZip = validateCharCount(user.getZip(), 1, 10); 
		boolean bCity = validateCharCount(user.getCity(), 1, 100); 
		boolean bPassword = validateCharCount(user.getPassword(), 1, 50); 
		
		//Simplest implementation: could be done better, probably 
		//bValidate is set to false when only 1 of the criteria was not fulfilled 
		if(Arrays.asList(fields).contains("user_name") && !bUserName) bValidate = false; 
		if(Arrays.asList(fields).contains("given_name") && !bGivenName) bValidate = false; 
		if(Arrays.asList(fields).contains("last_name") && !bLastName) bValidate = false; 
		if(Arrays.asList(fields).contains("address") && !bAddress) bValidate = false; 
		if(Arrays.asList(fields).contains("zip") && !bZip) bValidate = false; 
		if(Arrays.asList(fields).contains("city") && !bCity) bValidate = false; 
		if(Arrays.asList(fields).contains("password") && !bPassword) bValidate = false; 
		
		return bValidate; 
	}
	
	/**
	 * Check min/max length of the string 
	 * @param strValidate
	 * @param min
	 * @param max
	 * @return
	 */
	private boolean validateCharCount(String strValidate, int min, int max){ 
		boolean bValidate = true; 
		int strLength = strValidate.length(); 
		if(strLength > max || strLength < min){ 
			bValidate = false; 
		}
		
		return bValidate; 
	}
}
