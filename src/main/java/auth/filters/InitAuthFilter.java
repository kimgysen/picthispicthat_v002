package auth.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import auth.Auth;

/**
 * Initialize auth: applicable to all servlets 
 * @author kimgysen
 *
 */
public class InitAuthFilter implements Filter {
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request; 
		HttpServletResponse res = (HttpServletResponse) response; 
		
		//Ensure a session exist 
		HttpSession session = req.getSession( true ); 
		
		//Initialize session attribute 'loggedIn'
		if ( session.getAttribute("loggedIn") == null){
			session.setAttribute("loggedIn", false);
		}
		
		//Set servlet path for register / login redirection
		req.setAttribute("reqPath", req.getServletPath()); 
    	String redirPath = req.getParameter("from") == null || req.getParameter("from").equals("") ? "/home" : (String) req.getParameter("from"); 
    	req.setAttribute("redirPath", redirPath); 
		
		//Don't allow the user to access login / register pages when already logged in 
		if(Auth.isAuthenticated(req)){
			String pathInfo = req.getServletPath(); 
	 		if( pathInfo.equals("/login") || pathInfo.equals("/register") ){ 
	 			res.sendRedirect("home"); 
	 			return; 
	 		} 
		}
		req.setAttribute("visibility", "public");
		chain.doFilter(request, response);
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}
	
	@Override
	public void destroy() {}
	
}
