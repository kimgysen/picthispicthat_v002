package auth.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import auth.Auth;


/**
 * Verify the auth status and redirect the user to the login page when necessary  
 * Applicable to authentication protected pages 
 * @author kimgysen
 *
 */
public class AuthFilter implements Filter {
	 @Override
	 public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException { 
		 
		HttpServletRequest req = (HttpServletRequest) request; 
		HttpServletResponse res = (HttpServletResponse) response; 
		
        if (!Auth.isAuthenticated(req)) {
        	String strPath = req.getRequestURI().substring(req.getContextPath().length()); 
            res.sendRedirect(req.getContextPath() + "/login?from=" + strPath);
            return;
        }
		chain.doFilter(request, response); 
	 }
	 
	 @Override
	 public void init(FilterConfig config) throws ServletException { }
	 
	 @Override
	 public void destroy() { }
	
}
