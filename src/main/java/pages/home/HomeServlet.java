package pages.home;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.img.beans.Image;
import entities.img.repo.ImageRepository;

/**
 * Home page servlet 
 * @author kimg
 *
 */
@WebServlet("/home")
public class HomeServlet extends HttpServlet{
	
    private static final long serialVersionUID = 1;
    private static final int NR_PICS = 6;
    private static final int CATEGORY_ID = 0;
    private static final String filePath = "/images/";

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        List<Image> list = new ImageRepository().getRandom(NR_PICS, CATEGORY_ID); 
        req.setAttribute("pictures", list); 
        req.setAttribute("filePath", filePath); 
        req.getRequestDispatcher("WEB-INF/jsp/pages/template.jsp").forward(req, res); 
    }
}
