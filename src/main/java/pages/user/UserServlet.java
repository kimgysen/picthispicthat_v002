package pages.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.user.beans.User;

/**
 * Show User page profile 
 * @author kimg
 *
 */
@WebServlet("/user")
public class UserServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException { 
    	User user = (User) req.getSession().getAttribute("user"); 
    	req.setAttribute("user", user); 
    	
        req.getRequestDispatcher("/WEB-INF/jsp/pages/user.jsp").forward(req, res); 
    }
	
}
