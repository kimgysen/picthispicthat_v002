package pages.user;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.SerializationUtils;

import auth.Auth;
import auth.validator.RegisterValidator;
import entities.user.beans.User;
import entities.user.repo.UserRepository;

/**
 * User edit servlet 
 * @author kimg
 *
 */
@WebServlet("/user/edit")
public class UserEditServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	private static final String VIEW_NAME  = "/WEB-INF/jsp/pages/user_edit.jsp"; 
	private static final String MODEL_NAME = "form"; 
	private static final String ERROR_NAME = "error"; 
	
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException { 
        req.getRequestDispatcher("/WEB-INF/jsp/pages/user_edit.jsp").forward(req, res); 
    } 
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
    	
    	boolean bValidated = false; 
    	String strError = null; 
    	
    	//Deep copy is required to avoid modifying the original User object 
    	User user = (User) SerializationUtils.clone((Serializable) req.getSession().getAttribute("user")); 
    	user.setGivenName(req.getParameter("given_name"));
    	user.setLastName(req.getParameter("last_name"));
    	user.setAddress(req.getParameter("address")); 
    	user.setZip(req.getParameter("zip"));
    	user.setCity(req.getParameter("city"));
    	
    	RegisterValidator validator = new RegisterValidator(user); 
		bValidated = validator.validate(new String[]{"given_name", "last_name", "address", "zip", "city"}); 
		if(!bValidated) strError = "Invalid fields: make sure that all field are filled in, and that the max amount of characters is not exceeded!"; 
		
		if(bValidated){
			//Store the user 
			long id = new UserRepository().save(user); 
			
			if(id != 0){ 
				Auth.setUserSession(req, user);
				String strFrom = req.getParameter("from");
	            if (strFrom != null) {
	                res.sendRedirect(req.getContextPath() + "/user?from=" + strFrom);
	            } else {
	                res.sendRedirect(req.getContextPath() + "/user");
	            }
			}
			
		} else {
			//Send the user back with error message 
			final SimpleForm form = map(req); 
			form.setValid(false);
			req.setAttribute(MODEL_NAME, form);
			req.setAttribute(ERROR_NAME, strError);
			req.getRequestDispatcher(VIEW_NAME).forward(req, res); 
		}
		
    }
    
	/**
	 * Map request parameters to form fields 
	 * @param request
	 * @return
	 */
	private SimpleForm map(final HttpServletRequest request) {
	    SimpleForm form = new SimpleForm();
	    form.setLastName(request.getParameter("last_name"));
	    form.setUserName(request.getParameter("user_name"));
	    form.setAddress(request.getParameter("address"));
	    form.setZip(request.getParameter("zip"));
	    form.setCity(request.getParameter("city"));
	    
	    return form;
	}
	
	/**
	 * Class representing the user edit form 
	 * @author kimg
	 *
	 */
	public class SimpleForm implements Serializable {
	    private static final long serialVersionUID = -2756917543012439177L;
	    
	    private String lastName;
	    private String userName; 
	    private String address; 
	    private String zip; 
	    private String city; 
	    private boolean valid; 
	    
	    public String getLastName() {
	        return lastName; 
	    }
	    public void setLastName(String lastName) {
	        this.lastName = lastName;
	    }
	    public String getUserName() {
	        return userName;
	    }
	    public void setUserName(String userName) {
	        this.userName = userName;
	    }
	    public String getAddress(){
	    	return address; 
	    }
	    public void setAddress(String address){
	    	this.address = address; 
	    }
	    public String getZip(){
	    	return zip; 
	    }
	    public void setZip(String zip){
	    	this.zip = zip; 
	    }
	    public String getCity(){
	    	return city; 
	    }
	    public void setCity(String city){
	    	this.city = city; 
	    }
	    public void setValid(boolean valid){
	    	this.valid = valid; 
	    }
	    public boolean isValid(){
	    	return valid; 
	    }
	}	    
}
