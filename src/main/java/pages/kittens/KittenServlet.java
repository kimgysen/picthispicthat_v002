package pages.kittens;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.img.beans.Image;
import entities.img.repo.ImageRepository;

/**
 * Kittens page servlet
 * @author kimgysen
 *
 */
@WebServlet("/kittens")
public class KittenServlet extends HttpServlet {
    private static final long serialVersionUID = 1;
    private static final int NR_PICS = 6;
    private static final int CATEGORY_ID = 1;
    private static final String VISIBILITY = "private";
    private static final String filePath = "/images/kittens/";
    
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.setAttribute("visibility", VISIBILITY);
        List<Image> list = new ImageRepository().getRandom(NR_PICS, CATEGORY_ID);
        req.setAttribute("pictures", list);
        req.setAttribute("filePath", filePath);
        req.getRequestDispatcher("WEB-INF/jsp/pages/template.jsp").forward(req, res);
    }
}
