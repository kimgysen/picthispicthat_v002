package config;

/**
 * Database configuration file 
 * @author kimg
 *
 */
public class DbConfig { 
	
	//Set configuration here 
	static final String PORT 		= "3306"; 
	static final String DB_NAME 	= "picthispicthat"; 
	static final String HOST 		= "localhost"; 
	static final String URL			= "jdbc:mysql://" + HOST + ":" + PORT + "/" + DB_NAME; 
	static final String USER 		="root"; 
	static final String PASSWORD 	= ""; 
	static final String DB_TYPE 	= "mysql"; 
	
	//Convenience methods 
	public static String getUrl(){ 
		return DbConfig.URL; 
	} 
	
	public static String getUser(){
		return DbConfig.USER;
	}
	
	public static String getPassword(){
		return DbConfig.PASSWORD; 
	} 
	
	public static String getDbType(){
		return DbConfig.DB_TYPE; 
	}
}
