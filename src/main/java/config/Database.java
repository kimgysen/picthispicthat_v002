package config;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import config.DbConfig;


public class Database { 
	
	public static MysqlDataSource mysqlDS; 
	public static EntityManager entityMgr; 
	
	/**
	 * Allow multiple types of datasource if necessary 
	 * @return
	 */
	public static DataSource getDataSource(){
        //if("mysql".equals( DbConfig.getDbType()) ){ -- uncomment when adding multiple db-types
            return Database.getMySQLDataSource(); 
	}
	
	public static EntityManager getEntityManager(){
		if(entityMgr == null){ 
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("dbConn"); 
			entityMgr = emf.createEntityManager(); 
		} 
		return entityMgr; 
	} 
	
	/**
	 * Get DataSource for MySQL 
	 * @return
	 */
    public static DataSource getMySQLDataSource() { 
        if(mysqlDS == null) {
        	mysqlDS = new MysqlDataSource(); 
            mysqlDS.setURL( DbConfig.getUrl() );
            mysqlDS.setUser( DbConfig.getUser() );
            mysqlDS.setPassword( DbConfig.getPassword() );         	
        }
        
        return mysqlDS;
    }	
}
