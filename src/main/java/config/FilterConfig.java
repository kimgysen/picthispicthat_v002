package config;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import auth.filters.AuthFilter;
import auth.filters.InitAuthFilter;

/**
 * Programmatically bind filters, configure paths 
 * @author kimgysen
 *
 */
@WebListener
public class FilterConfig implements ServletContextListener{
	
	@Override
	public void contextInitialized(ServletContextEvent event){
		ServletContext context = event.getServletContext();
		
		//Authentication init filter 
		FilterRegistration.Dynamic initAuth = context.addFilter("InitAuthFilter", new InitAuthFilter());
		initAuth.addMappingForUrlPatterns( null, false, "*" );
		
		//Authentication filter 
		FilterRegistration.Dynamic authentication = context.addFilter("authFilter", new AuthFilter());
		authentication.addMappingForUrlPatterns( null, false, "/kittens", "/computers", "/images/kittens/*", "/images/computers/*", "/user/*" );
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent event) { }
	 
}