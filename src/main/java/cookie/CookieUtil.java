package cookie;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieUtil {
	
	/**
	 * Add cookie to req header 
	 * @param res
	 * @param name
	 * @param value
	 * @param maxAge
	 */
	public static void addCookie(HttpServletResponse res, String name, String value, int maxAge){
		Cookie cookie = new Cookie(name, value); 
		cookie.setPath("/"); 
		cookie.setMaxAge(maxAge); 
		res.addCookie(cookie);
	}
	
	/**
	 * Empty cookie 
	 * @param res
	 * @param name
	 */
	public static void removeCookie(HttpServletResponse res, String name){
		addCookie(res, name, null, 0); 
	}
	
	/**
	 * Get the cookie value 
	 * @param req
	 * @param cookieName
	 * @return
	 */
	public static String getCookieValue(HttpServletRequest req, String cookieName) {
		Cookie[] cookies = req.getCookies(); 
	    if (cookies != null) {
	        for (Cookie cookie : cookies) {
	            if (cookieName.equals(cookie.getName())) {
	                return cookie.getValue();
	            }
	        }
	    }
	    return null;		
	 }
}
